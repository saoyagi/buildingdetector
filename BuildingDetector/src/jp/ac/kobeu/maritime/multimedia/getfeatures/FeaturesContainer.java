package jp.ac.kobeu.maritime.multimedia.getfeatures;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import net.arnx.jsonic.JSON;
import net.semanticmetadata.lire.imageanalysis.LireFeature;
import net.semanticmetadata.lire.imageanalysis.sift.Feature;

/*
 * 1つのファイルの特徴量をいれておくためのクラス
 * 2013-01-23 Saizo Aoyagi
 */

class FeaturesContainer{
	private HashMap<String, double[]> features = new HashMap<String, double[]>();
	private String fileName = "";
	private String filePath = "";
	private Boolean isBuilding = null; //建物: true / そうでない:false
	public Boolean getIsBuilding() {
		return isBuilding;
	}


	public void setIsBuilding(Boolean isBuilding) {
		this.isBuilding = isBuilding;
	}
	private String type = ""; //画像タイプ

	public void addFeature(String key, double[] feature){
		this.features.put(key, feature);
	}
	
	
	//特徴量クラスを渡して保存
	public void addFeature(LireFeature feature){
		String key = feature.getClass().getName();
		
		this.features.put(key, feature.getDoubleHistogram());
	}
	
	//画像と特徴量クラスを渡して保存
	public void extractAndAddFeature(LireFeature feature, BufferedImage img){
		String key = feature.getClass().getSimpleName();
		feature.extract(img);
		this.features.put(key, feature.getDoubleHistogram());
	}
	
	public double[] getFeature(String key){
		
		return this.features.get(key);
	}
	
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	//Json形式に変換する
	public String toJson (){
		return JSON.encode(this);
	}
	public String toString(){
		return toJson();
	}
}