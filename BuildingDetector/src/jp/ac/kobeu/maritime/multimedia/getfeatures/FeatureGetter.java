package jp.ac.kobeu.maritime.multimedia.getfeatures;

import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;

import net.arnx.jsonic.JSON;
import net.semanticmetadata.lire.imageanalysis.LireFeature;
import net.semanticmetadata.lire.utils.FileUtils;





/*
 * lireでinddxを生成せずに、画像の特徴量だけもとめるクラス
 * 2013-01-23 Saizo Aoyagi
 */
public class FeatureGetter {
	
	//LIREのいろんな特徴量のクラスがはいっているパッケージの名前
	public static final String LIRE_FEATURE_PACKAGE_NAME = "net.semanticmetadata.lire.imageanalysis";
	
	//ターゲットにする特徴量 lireのクラス名を指定
	public static final String[] TARGET_FEATURES = {"EdgeHistogram", "ScalableColor", "ColorLayout"};
	
	
	//このプログラムのあるパス
	final static String CURRENT_PATH = new File("").getAbsolutePath();
	

	//画像データをいれておくところ
	public final static String TEST_BUILDING_DIR = CURRENT_PATH +"/" +"testdataset/building";
	public final static String TEST_NON_BUILDING_DIR = CURRENT_PATH +"/" +"testdataset/non-building";
	public final static String BUILDING_FILE_DIR = CURRENT_PATH +"/dataset/building";
	public final static String NON_BUILDING_FILE_DIR = CURRENT_PATH +"/dataset/non-building";

	
	//結果書き込み用のファイル(画像のパスと特徴量をセットで保存する)のパス
	public static final String RESULT_FILE="result.csv"; //判別器生成用
	public static final String TEST_FILE="test.csv"; //判別テスト用
	

	//エントリポイント
	public static void main(String[] args) throws IOException {
		//実行したい処理に合わせていらんやつをコメントアウトすること
		
		
		//エッジヒストグラムの解読用
		//learnEdgeHistgram(); 
		
		//特徴量を計算して結果をcsvで書き出す
		makeTestData();
		
		//RandomForestかける
		//todo 実装
	
	
	}
	
	//edgehistgramの中身を調べる
	public static void learnEdgeHistgram() throws IOException{
		String path =  CURRENT_PATH +"/test2";
		

		ArrayList<FeaturesContainer> featuresContainers = new ArrayList<FeaturesContainer>();
	
		File dirFile = new File(path);
		ArrayList<String> images =FileUtils.getAllImages(dirFile, true);
		featuresContainers = getFeatureContaniersFromImages(images, false, featuresContainers);
	}
	
	//学習用データとテスト用データを出力する
	public static void makeTestData() throws IOException{
		//各ファイルの各特徴量を格納する変数
		ArrayList<FeaturesContainer> featuresContainers = new ArrayList<FeaturesContainer>();
		
		//建物
		File dirFile = new File(BUILDING_FILE_DIR);
		ArrayList<String> images =FileUtils.getAllImages(dirFile, true);
		//イメージパス一覧から特徴量コンテナ一覧を取得する
		featuresContainers = getFeatureContaniersFromImages(images, true, featuresContainers);//建物

		//建物じゃない
		dirFile = new File(NON_BUILDING_FILE_DIR);
		images =FileUtils.getAllImages(dirFile, true);
		//イメージパス一覧から特徴量コンテナ一覧を取得する
		featuresContainers = getFeatureContaniersFromImages(images, false, featuresContainers); //建物じゃない		
		
		//csvファイル出力
		writeCSV(featuresContainers, RESULT_FILE);
		
		
		//テスト用データ
		featuresContainers = new ArrayList<FeaturesContainer>();
		dirFile = new File(TEST_NON_BUILDING_DIR);
		images =FileUtils.getAllImages(dirFile, true);
		featuresContainers = getFeatureContaniersFromImages(images, false, featuresContainers); //テスト用 建物じゃない	
		
		dirFile = new File(TEST_BUILDING_DIR);
		images =FileUtils.getAllImages(dirFile, true);
		featuresContainers = getFeatureContaniersFromImages(images, true, featuresContainers); //テスト用	建物
		
		writeCSV(featuresContainers, TEST_FILE);
		
	}
	
	//イメージパス一覧から特徴量コンテナ一覧を取得するメソッド
	public static ArrayList<FeaturesContainer> getFeatureContaniersFromImages(ArrayList<String> images, Boolean isBuilding, ArrayList<FeaturesContainer> featuresContainers){
		
		
		for (Iterator<String> it = images.iterator(); it.hasNext(); ) {
			String imageFilePath = it.next();
			System.out.println("Analysing " + imageFilePath);
			try {
				//画像を1枚取得
				BufferedImage img = ImageIO.read(new FileInputStream(imageFilePath));
				//特徴量を計算 ここの第三引数は、建物かどうかの真偽値
				featuresContainers.add(getFeatures(img, imageFilePath, isBuilding));
			} catch (Exception e) {
				System.err.println("Error reading image or creating document.");
				e.printStackTrace();
			}	
		}
		
		return featuresContainers;
	}
	
	
	//画像1枚の特徴量を計算するメソッド
	public static FeaturesContainer getFeatures(BufferedImage img, String filePath ,Boolean isBuilding) throws IOException{
		
		//特徴量のインスタンス
		LireFeature lireFeature;
		//特徴量のクラス
		Class<?> lireFeatureClass =null;
		
		//特徴量を格納しておく変数
		FeaturesContainer resultContainer = new FeaturesContainer();
		
		//建物かどうかのセット
		resultContainer.setIsBuilding(isBuilding);
		
		resultContainer.setFilePath(filePath);

		String fileName = filePath.substring(filePath.lastIndexOf("/")+1);
		resultContainer.setFileName(fileName );
		
		//クラスローダ
		ClassLoader loader = ClassLoader.getSystemClassLoader();
		
		
		for (int i =0; i < TARGET_FEATURES.length; i++){	
	
			
			//各特徴量を計算するクラスを取得
			try {
				lireFeatureClass = loader.loadClass(LIRE_FEATURE_PACKAGE_NAME +"."+ TARGET_FEATURES[i]);
				lireFeature = null;
				try {
					lireFeature = (LireFeature) lireFeatureClass.newInstance();
					
					//対象画像と特徴量のクラスのセット
					resultContainer.extractAndAddFeature(lireFeature, img);
					
					
					
					//コンソール表示
					System.out.println(TARGET_FEATURES[i] + resultContainer.getFeature(TARGET_FEATURES[i]).length + " items " + JSON.encode(resultContainer.getFeature(TARGET_FEATURES[i])));
					
				} catch (InstantiationException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO 自動生成された catch ブロック
					e.printStackTrace();
				}
				
			} catch (ClassNotFoundException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				lireFeatureClass = null;
			}
			
			
		}	
		

		//FeatureContanierのインスタンスを返す
		return resultContainer;
	}
	
	//特徴量コンテナ一覧をcsv形式で出力するメソッド
	public static void writeCSV(ArrayList<FeaturesContainer> featuresContainers, String resultPath) throws IOException{
		if (featuresContainers.size() == 0 ) throw new Error("特徴量が計算されていません。"); 
		
		//文字列生成用
		ArrayList<String> stringArray = new ArrayList<String>();

		//結果書き込み用のファイルの準備　
		//bufferedWriter = new BufferedWriter(new FileWriter(RESULT_FILE,true)); //追記モード
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(resultPath,false)); //上書きモード
		
		//一時保存用
		FeaturesContainer tmpContainer = featuresContainers.get(0);
		double[] tmpFeature = null;
		int itemNumOfFeatureArray = 0;
		
		//一番上の行を作成
		stringArray.add("n");
		stringArray.add("FileName");
		stringArray.add("IsBuilding");
		for (int i =0; i < TARGET_FEATURES.length; i++){	
			tmpFeature = tmpContainer.getFeature(TARGET_FEATURES[i]);
			itemNumOfFeatureArray = tmpFeature.length;
			for(int j = 0; j < itemNumOfFeatureArray; j++){
				stringArray.add(TARGET_FEATURES[i] + Integer.toString(j));
			}
		}
		bufferedWriter.write(StringUtils.join(stringArray,","));
		bufferedWriter.flush();
		bufferedWriter.newLine();
		
		
		
		
		//実際のデータを書いていく
		for(int i =0; i < featuresContainers.size() ; i++ ){
			stringArray = new ArrayList<String>();
			
			tmpContainer = featuresContainers.get(i);
			
			//行番号
			stringArray.add(Integer.toString(i+1));
			//ファイル名
			stringArray.add(tmpContainer.getFileName());
			
			//建物かどうか
			String isBuilding = "na";
			if (tmpContainer.getIsBuilding() != null){
				isBuilding = tmpContainer.getIsBuilding().toString();
			}
			stringArray.add(isBuilding);
			
			for (int j =0; j < TARGET_FEATURES.length; j++){	
				tmpFeature = tmpContainer.getFeature(TARGET_FEATURES[j]);
				itemNumOfFeatureArray = tmpFeature.length;
				for(int k = 0; k < itemNumOfFeatureArray; k++){
					stringArray.add(Double.toString(tmpFeature[k]));
				}
			}
			
			bufferedWriter.write(StringUtils.join(stringArray,","));
			bufferedWriter.flush();
			bufferedWriter.newLine();
		}
		
		bufferedWriter.close();
		
	}

}


